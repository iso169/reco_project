from django.shortcuts import render, redirect
from django.contrib.auth import get_user_model
from django.contrib import auth
from django.views import View
from celery.result import AsyncResult
from datasource.models import datasource_user
from .tasks import create_user_database

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# Create your views here.

User = get_user_model()

def signup(request):
    
    if request.method == 'POST':
        if request.POST['password1'] == request.POST['password2']:
            try:
                user = User.objects.get(phone=request.POST['phone'])
                return render(request, 'accounts/signup.html', {'error': 'User Exist!'})
            except User.DoesNotExist:
                user = User.objects.create_user(email=request.POST['email'], phone=request.POST['phone'], username=request.POST['username'], password=request.POST['password1'])
                user_datasource_name = datasource_user.objects.get(user_id=user.id).datasource_name
                task = create_user_database.delay(user_datasource_name)
                auth.login(request, user)
                return render(request, 'accounts/signup.html', {'task_id': task.task_id})
        else:
            return render(request, 'accounts/signup.html', {'error': 'Passwords not match!!!'})
    else:
        return render(request, 'accounts/signup.html')


def sign(request):
    return render(request, 'accounts/new_signup.html')


class NewSignup(APIView):
    
    def post(self, request):
        data = {}
        try:
            user = User.objects.get(phone=request.data['phone'])
            data['error'] = "User Exist!"
            return Response(data, status=status.HTTP_409_CONFLICT)
        except User.DoesNotExist:
            user = User.objects.create_user(phone=request.data['phone'])
            user_datasource_name = datasource_user.objects.get(user_id=user.id).datasource_name
            task = create_user_database.delay(user_datasource_name)
            data['resault'] = "User Created!!"
            return Response(data, status=status.HTTP_201_CREATED)




def login(request):
    if request.method == 'POST':
        user = auth.authenticate(username=request.POST['phone'], password=request.POST['password'])
        if user is not None:
            auth.login(request, user)
            return redirect('log-in')
        else:
            return(request, 'accounts/login.html', {'error': 'The phone number or password is incorrect!'})
    else:
        return render(request, 'accounts/login.html')

def logout(request):
    if request.method == 'POST':
        auth.logout(request)
        return redirect('home')



class DataBaseCreationStatus(APIView):

    def post(self, request):
        task = AsyncResult(request.data['task_id'])
        result = {
            "state": task.state
        }
        if task.state == "STARTED":
            return Response(result, status=status.HTTP_100_CONTINUE)
        elif task.state == "SUCCESS":
            result["result"] = task.result
            task.forget()
            return Response(result, status=status.HTTP_200_OK)
        elif task.state == "FAILURE":
            result["reason"] = str(task.info)
            task.forget()
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        elif task.state == "REVOKED":
            task.forget()
            return Response(result, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(result, status=status.HTTP_404_NOT_FOUND)



from django.http import HttpResponse
def goozin(request):
    return HttpResponse('goozina')