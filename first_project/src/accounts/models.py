from django.db import models

# Create your models here.
from django.contrib.auth.base_user import (
    AbstractBaseUser,
    BaseUserManager,
)


class CustomUserManager(BaseUserManager):
    def create_user(self, phone, password="123456789"):
#        if not email:
#            raise ValueError("User must have an email!")

        if not phone:
            raise ValueError("User must have a phone number!")

#        if not password:
#            raise ValueError("User must enter a password!")

        user = self.model(
#            email=self.normalize_email(email),
            phone=phone,
#            username=username,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user


    def create_staff(self, phone, password="123456789"):
        user = self.create_user(
#            email,
            password=password,
            phone=phone,
#            username=username,
        )
        
        user.is_staff = True
        user.save(using=self._db)
        return user


    def create_superuser(self, phone, password="123456789"):
        user = self.create_user(
#            email,
            password=password,
            phone=phone,
#            username=username,
        )
        
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user



class CustomUserModel(AbstractBaseUser):
    username = models.CharField(
        max_length=60,
        null=True,
#        unique=True,
#        default='iso1669',
    )
    email = models.EmailField(
        verbose_name='email',
        max_length=255,
        null=True,
#        unique=True,
#        default='esm.dsh@gmail.com'
    )
    phone = models.CharField(
        verbose_name='phone number',
        unique=True,
        max_length=15,
        default='00000000000000'
    )
    first_name = models.CharField(
        verbose_name='first_name',
        max_length=60,
        blank=True,
        null=True,
    )
    last_name = models.CharField(
        verbose_name='last_name',
        max_length=60,
        blank=True,
        null=True,
    )
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    date_joined = models.DateTimeField(
        verbose_name='date joined',
        auto_now_add=True,
    )

    objects = CustomUserManager()

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.phone

    def has_perm(self, perm, obj=None):
        return self.is_superuser
    
    def has_module_perms(self, app_label):
        return True

    def get_full_name(self):
        return self.first_name + ' ' + self.last_name

    def get_short_name(self):
        return self.first_name
    
    @property
    def active(self):
        return self.is_active

    @property
    def staff(self):
        return self.is_staff

    @property
    def superuser(self):
        return self.is_superuser