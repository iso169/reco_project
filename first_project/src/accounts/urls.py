from django.urls import path, include
from .views import (
    signup,
    login,
    logout,
    DataBaseCreationStatus,
    NewSignup,
    sign,
    goozin,
)

urlpatterns = [
#    path('', signup, name='home'),
    path('login/', login, name='log-in'),
    path('logout/', logout, name='log-out'),
    path('user_database_creation_status/', DataBaseCreationStatus.as_view(), name='mabna'),
    path('new_signup/', NewSignup.as_view(), name='new_signup'),
    path('signup/', sign, name='home'),
    path('goozin/', goozin, name='goozin'),
]