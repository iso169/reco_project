from __future__ import absolute_import, unicode_literals
from celery import shared_task
from sqlalchemy import create_engine
from time import sleep


@shared_task
def create_user_database(user_datasource_name):
    
    sleep(60)
    with create_engine(
        "postgresql://postgres:1234567899876543210@localhost/postgres",
        isolation_level='AUTOCOMMIT'
    ).connect() as conn:
        conn.execute('CREATE DATABASE datasource_name' + str(user_datasource_name))

    
    
#    count = 0
#    while True:
#        count = count + 1
#        
#        if count == 1000000000:
#            break
    return f'Database datasource_name {user_datasource_name} created!'
#    return count