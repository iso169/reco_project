from random import randint
from django.db.models.signals import post_save
from django.contrib.auth import get_user_model
from .models import datasource_user

User = get_user_model()

def random_with_N_digits(n):

    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def datasource_signal(sender, instance, created, **kwargs):
    
    if created:
        user_datasource_name = random_with_N_digits(10)
        datasource_user.objects.update_or_create(
        user_id=instance.id,
        datasource_name=user_datasource_name,
        )
    

post_save.connect(datasource_signal, sender=User, dispatch_uid='datasource_signal.datasource_name')