from django.apps import AppConfig


class DatasourceConfig(AppConfig):
    name = 'datasource'
    
    def ready(self):
        super(DatasourceConfig, self).ready()

        import datasource.signals
