from django.contrib import admin
from django.urls import path
from .views import (
    CreateTable,
    TableCreationStatus,
)

urlpatterns = [
    path('', CreateTable.as_view()),
    path('table_creation_status/', TableCreationStatus.as_view(), name='gooz'),
]