from __future__ import absolute_import, unicode_literals
from celery import shared_task
from .models import datasource_user
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData
from time import sleep

@shared_task
def create_table(table_name):
    
    sleep(60)
    
    DB_URL_main = "postgresql://postgres:1234567899876543210@localhost/"
    datasource_names = datasource_user.objects.values_list('datasource_name', flat=True).iterator()
    for name in datasource_names:
        DB_URL = DB_URL_main + 'datasource_name' + name
        engine = create_engine(DB_URL)
        meta = MetaData()
        tabl = Table(
            table_name, meta, 
            Column('id', Integer, primary_key = True), 
            Column('name', String), 
            Column('lastname', String), 
            )
        meta.create_all(engine)

    return f'Tables {table_name} created.'