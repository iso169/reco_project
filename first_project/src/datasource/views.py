from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .tasks import create_table
from celery.result import AsyncResult
# Create your views here.

class CreateTable(APIView):
    
    def post(self, request):
        task = create_table.delay(request.data['table'])
        task_info = f"task_id={task.task_id}, task status={task.status}, task state={task.state}"
        return Response("Your tables will create as soon as possible. It's all up to us! "+task_info, status=status.HTTP_201_CREATED)


class TableCreationStatus(APIView):

    def post(self, request):
        task = AsyncResult(request.data['task_id'])
        result = {
            "state": task.state
        }
        if task.state == "STARTED":
            return Response(result, status=status.HTTP_100_CONTINUE)
        elif task.state == "SUCCESS":
            result["result"] = task.result
            task.forget()
            return Response(result, status=status.HTTP_200_OK)
        elif task.state == "FAILURE":
            result["reason"] = str(task.info)
            task.forget()
            return Response(result, status=status.HTTP_400_BAD_REQUEST)
        elif task.state == "REVOKED":
            task.forget()
            return Response(result, status=status.HTTP_404_NOT_FOUND)
        else:
            return Response(result, status=status.HTTP_404_NOT_FOUND)