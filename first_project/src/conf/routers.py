class AuthRouter:
    """
    A router to control all database operations on models in the
    auth, contenttypes and reco_users_db applications.
    """
    route_app_labels = {
        'auth',
        'contenttypes',
        'admin',
        'accounts',
    }

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth, contenttypes and accounts models go to reco_users_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'reco_users_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth, contenttypes and accounts models go to reco_users_db.
        """
        if model._meta.app_label in self.route_app_labels:
            return 'reco_users_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth, contenttypes and accounts apps is
        involved.
        """
        if (
            obj1._meta.app_label in self.route_app_labels or
            obj2._meta.app_label in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth, contenttypes and accounts apps only appear in the
        'reco_users_db' database.
        """
        if app_label in self.route_app_labels:
            return db == 'reco_users_db'
        return None



class OtherRouter:
    """
    A router to control all database operations on models in the
    auth, contenttypes and reco_users_db applications.
    """
    route_app_labels = {
        'auth',
        'contenttypes',
        'admin',
        'accounts',
    }

    def db_for_read(self, model, **hints):
        """
        Attempts to read auth, contenttypes and accounts models go to reco_users_db.
        """
        if model._meta.app_label not in self.route_app_labels:
            return 'default'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write auth, contenttypes and accounts models go to reco_users_db.
        """
        if model._meta.app_label not in self.route_app_labels:
            return 'default'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the auth, contenttypes and accounts apps is
        involved.
        """
        if (
            obj1._meta.app_label not in self.route_app_labels and
            obj2._meta.app_label not in self.route_app_labels
        ):
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the auth, contenttypes and accounts apps only appear in the
        'reco_users_db' database.
        """
        if app_label not in self.route_app_labels:
            return db == 'default'
        return None